package tka3.letmein;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LetmeinApplication {

    public static void main(String[] args) {
        SpringApplication.run(LetmeinApplication.class, args);
    }

}
